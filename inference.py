import pandas as pd
import mlflow.sklearn
import os
from dotenv import load_dotenv
import uvicorn
from fastapi import FastAPI, File, UploadFile, HTTPException
import html
import pymorphy2
import regex
import nltk
from nltk.corpus import stopwords
from string import punctuation

import numpy as np
from scipy.special import softmax

morph = pymorphy2.MorphAnalyzer()
nltk.download("stopwords")
russian_stopwords = stopwords.words("russian")
blacklist = set(punctuation)
blacklist.update(["«", "»"])

load_dotenv()

app = FastAPI()

os.environ["MLFLOW_S3_ENDPOINT_URL"] = os.getenv("MLFLOW_S3_ENDPOINT_URL")


def preprocess(text):
    """returns a list, consisting of the lemmas from the input string"""
    text = html.unescape(text)
    text = text.replace("-", " ")
    text = text.replace("/", " ")
    text = regex.sub(r"[\s\xa0\u202F\uFEFF]+", " ", text)

    words = text.split()  # разбиваем текст на слова
    res = []
    for word in words:
        word = "".join(
            c for c in word if (c not in blacklist and c.isdigit() is False)
        )  # удаляем знаки препинания и цифры
        if (
            len(word) >= 3
        ):  # для всех слов, состоящих из трех и более букв, во множество ответов записываем нормальную форму слова
            word = morph.parse(word)[0].normal_form
            res.append(word)

    return (" ").join(res)


class Model:
    def __init__(self, model_name, model_stage):
        """
        To initialize the model
        model_name: Name of the model in registry
        model_stage: Stage of the model
        """
        # Load the model from Registry
        self.model = mlflow.sklearn.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        """
        To use the loaded model to make predictions on the data
        data: text to perform predictions
        """
        pred = self.model.predict(data)
        return pred

    def predict_top5(self, data):
        """
        To use the loaded model to predict top 5 classes with their probabilities of the input text
        data: text to perform predictions
        """
        decision_scores = self.model.decision_function(data)[0]
        top5_indices = np.argsort(decision_scores)[::-1][:5]
        top5_probabilities = softmax(decision_scores[top5_indices])
        classes = self.model.classes_
        prediction = dict()
        for i in range(len(top5_indices)):
            prediction[classes[top5_indices[i]]] = round(top5_probabilities[i], 4)
        return prediction


model = Model("doctors_svm_classifier", "Staging")

@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".txt"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        text = pd.read_csv(file.filename, header=None).loc[0][0]
        text_lem = preprocess(text)
        pred = model.predict_top5([text_lem])
        return (text, pred)
    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only txt files accepted."
        )


if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
